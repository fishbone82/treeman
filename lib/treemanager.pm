package treemanager;
use Dancer ':syntax';
use MyTree;
use Template;
use DBI;

my $DBH;

get '/' => sub {    
    my $dbh = get_dbh();
    return "Database connection error: $dbh" unless ref $dbh;
    template 'index.tt' => { 
        page_title  => 'Tree Manager', 
        tree        => MyTree->new(dbh => $dbh) 
    };
};

post '/add' => sub {
    my $dbh = get_dbh();
    return "Database connection error: $dbh" unless ref $dbh;
    my $add_id = param("add_id");
    my $error = ($add_id =~ m!^\d+$!) ? undef : 'PID must be integer!';
    my $tree = MyTree->new(dbh => $dbh);
    unless($error) {        
        my $res = $tree->add_node($add_id);
        $error = $res->{error} if ref $res;
    }
    if ($error) {
        template 'index.tt' => { 
            page_title  => 'Tree Manager: error adding a new node', 
            tree        => $tree, 
            add_error   => "Error: $error",
        };
    } else {
        redirect('/');
    }
};

post '/del' => sub {
    my $dbh = get_dbh();
    return "Database connection error: $dbh" unless ref $dbh;
    my $del_id = param("del_id");
    my $error = ($del_id =~ m!^\d+$!) ? undef : 'ID must be integer!';
    my $tree = MyTree->new(dbh => $dbh);
    unless($error) {
        my $res = $tree->delete_node(param("del_id"));
        $error = $res->{error} if ref $res;
    }
   
    if ($error) {
        template 'index.tt' => { 
            page_title      => 'Tree Manager: error deleting a node', 
            tree            => $tree, 
            delete_error    => "Error: $error"
        };
    } else {
        redirect('/');
    }  
};

sub get_dbh {
    return $DBH if $DBH && $DBH->ping(); # No need to connect if we already have an alive $DBH
    my $db_params = config->{db} or return "There is no 'db' section in config!";
    $DBH = DBI->connect( 
        $db_params->{connect_string}, 
        $db_params->{user}, 
        $db_params->{password}
    );
    return $DBH || $DBI::errstr;
}

true;
