package MyTree;
use strict;
use warnings;

sub new {
    my ($class, %p) = @_;
    my $self = { 
        dbh => $p{dbh}
    };
    bless $self, $class;
    $self->_load_from_db();
    return $self;
}

sub _load_from_db {
    my $self = shift;
    return if $self->{is_loaded};
    
    my $sth = $self->{dbh}->prepare("SELECT * from nodes");
    $sth->execute;
    
    my $raw_data = $sth->fetchall_hashref('id');

    # Getting structure
    foreach my $node (sort {$a->{id} <=> $b->{id}} sort {($a->{parent_id} || 0) <=> ($b->{parent_id} || 0)} values %{$raw_data}) {
        my $parent_id = $node->{parent_id} || 'root';
        $raw_data->{$parent_id} = {id => 'root'} unless $raw_data->{$parent_id};
        push @{$raw_data->{$parent_id}->{chld}}, $node;
    }
    $self->{struct} = $raw_data->{root}->{chld} || [];
    $self->{raw_data} = $raw_data;

    # creating layers
    my $layers = [];
    _create_layers($layers, $self->{struct}, 0);
    $self->{layers} = $layers;
    $self->{is_loaded} = 1;
}

sub _create_layers {
    my ($dst, $data, $level) = @_;
    foreach my $node (@{$data}) {
        my $copy = {id => $node->{id}, parent_id => $node->{parent_id}};
        push @{$dst->[$level]}, $copy;                
        _create_layers($dst, $node->{chld}, $level + 1) if $node->{chld};
    }
}

sub add_node {
    my ($self, $parent_id) = @_;
    return { error => 'Invalid parent node', value => $parent_id } unless $self->get_node($parent_id || 'root');
    my $sth = $self->{dbh}->prepare("INSERT INTO nodes (parent_id) values(?)");    
    my $rv = $sth->execute($parent_id);
    return $rv == 1 ? $sth->{mysql_insertid} : {error => 'Unknown database error', value => $parent_id};
}

sub get_node {
    my ($self, $node_id) = @_;
    my ($node) = grep { $_->{id} && $_->{id} eq $node_id } values %{ $self->{raw_data} };
    $node = $self->{raw_data}->{root} if $node_id eq 'root';
    return $node;
}

sub delete_node {
    my ($self, $node_id) = @_;
    return { error => 'Undefined node_id', value => undef} unless $node_id;
    my $node = $self->get_node($node_id);
    return { error => 'Invalid node', value => $node_id } unless $node;
    my $joined_ids = join(',', _get_children_ids($node));
    $self->{dbh}->do("DELETE FROM nodes WHERE id IN ($joined_ids)");    
}

sub _get_children_ids {
    my $node = shift;
    my @ids = ($node->{id});
    if ($node->{chld}) {
        push @ids, _get_children_ids($_) foreach @{$node->{chld}};
    }
    return @ids;
}

1;